
#include "blc_program.h"
#include <string.h>
#include <libwebsockets.h>

static int callback_http(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len) {
    switch (reason) {
        case LWS_CALLBACK_CLIENT_WRITEABLE:
            printf("connection established\n");
            
        case LWS_CALLBACK_HTTP: {
            char *requested_uri = (char *) in;
            printf("requested URI: %s\n", requested_uri);
            
            if (strcmp(requested_uri, "/") == 0) {
                char const *universal_response = "Hello, World!";
                lws_write(wsi, (unsigned char*)universal_response, strlen(universal_response), LWS_WRITE_HTTP);
                break;
                
            } else {
                // try to get current working directory
                char cwd[1024];
                char *resource_path;
                
                if (getcwd(cwd, sizeof(cwd)) != NULL) {
                    // allocate enough memory for the resource path
                    resource_path = (char*)malloc(strlen(cwd)
                                           + strlen(requested_uri));
                    
                    // join current working directory to the resource path
                    sprintf(resource_path, "%s%s", cwd, requested_uri);
                    printf("resource path: %s\n", resource_path);
                    
                    char *extension = strrchr(resource_path, '.');
                    char *mime;
                    
                    // choose mime type based on the file extension
                    if (extension == NULL) {
                        mime = "text/plain";
                    } else if (strcmp(extension, ".png") == 0) {
                        mime = "image/png";
                    } else if (strcmp(extension, ".jpg") == 0) {
                        mime = "image/jpg";
                    } else if (strcmp(extension, ".gif") == 0) {
                        mime = "image/gif";
                    } else if (strcmp(extension, ".html") == 0) {
                        mime = "text/html";
                    } else if (strcmp(extension, ".css") == 0) {
                        mime = "text/css";
                    } else {
                        mime = "text/plain";
                    }
                    lws_serve_http_file(wsi, resource_path, mime, NULL, 0);
                    
                }
            }
            
            // close connection
            
            lws_close_reason(wsi, LWS_CLOSE_STATUS_GOINGAWAY, (unsigned char *)"seeya", 5);        }
        default:
            printf("unhandled callback '%d'\n", reason);
            break;
    }
    
    return 0;
}

static struct lws_protocols protocols[] = {
    /* first protocol must always be HTTP handler */
    {
        "http-only",    /* name */
        callback_http,  /* callback */
        0             ,  /* per_session_data_size */
        0, //rx_buffer_size
        0, //	unsigned int id;
        NULL, //void *user;
        0, 	//size_t tx_packet_size;

        
    },
    {
        NULL, NULL, 0   /* End of list */
    }
    
};


int main(int argc, char **argv) {
    // server url will be http://localhost:9000
    struct lws_context *context;
    struct lws_context_creation_info context_info;

    CLEAR(context_info);
    context_info.port=9000;
    context_info.protocols=protocols;
    
    context = lws_create_context(&context_info);

    
    if (context == NULL) {
        fprintf(stderr, "libwebsocket init failed\n");
        return -1;
    }
    
    printf("starting server...\n");
    
    // infinite loop, to end this server send SIGTERM. (CTRL+C)
    BLC_COMMAND_LOOP(0){
        lws_service(context, 1000);
        fprintf(stderr, "Nothing happened\n");
    }
    
    lws_context_destroy(context);
    
    return 0;
}
