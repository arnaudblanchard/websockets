Install
=======

OSX
===

    brew install libwebsockets


Other
=====

- See [tutorial](https://medium.com/@martin.sikora/libwebsockets-simple-http-server-2b34c13ab540)

